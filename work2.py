# -*- coding: utf-8 -*-
# 031904138-王绮雯
from bs4 import BeautifulSoup
import requests

# 半角字符转全角字符(参考林同学解决方式）
def chartrans(i):
	return chr(ord(i) + 65248)

# 半角字符串转全角字符串(参考林同学解决方式）
def strtrans(s):
	new = ''
	s = str(s)
	for i in s:
		new += chartrans(i)
	return new


# 使用requests爬取网页
def getHTMLText(url):
	try:
		headers = {
			"User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.77 Safari/537.36"
		}

		# 使用requests
		req = requests.get(url=url, headers=headers)
		req.encoding = 'gbk'
		data = req.text
		return data
	except Exception as err:
		print(err)


# 解析html页面内容
def parsePage(html):

	soup = BeautifulSoup(html, 'html.parser')
	body = soup.find("tbody")    #解析 html页面内容得到
	lis = body.find_all("tr")

	data = []
	try:
		for i in range(len(lis)):
			tr = lis[i]
			td = tr.find_all("td")
			city = td[0].text		#观察html页面内容得到
			AQI = td[1].text
			PM = td[2].text
			SO2 = td[3].text
			NO2 = td[4].text
			CO = td[5].text
			pollution = td[8].text.strip()
			data.append([city,AQI,PM,SO2,NO2,CO,pollution])
		return data
	except Exception as err:
		print(err)


def printlist(list):
	tplt = "{0}\t\t{1}\t\t{2}\t\t{3}\t\t{4}\t\t{5}\t\t{6}\t\t{7}"  #调整输出格式
	print(tplt.format("序号", "城市", "AQI", "PM2.5", "SO2", "NO2", "CO", "首要污染物", chr(12288)))
	for items in list:
		city = items[0]  # 观察html页面内容得到
		AQI = items[1]
		PM = items[2]
		SO2 = items[3]
		NO2 = items[4]
		CO = items[5]
		pollution = items[6]
		print(tplt.format(city,AQI,PM,SO2,NO2,CO,pollution, chr(12288)))


if __name__ == "__main__":
	url = "https://datacenter.mee.gov.cn/aqiweb2/"
	html = getHTMLText(url)  # 使用requests爬取网页
	list = parsePage(html)  # 解析网页内容得到列表
	printlist(list)  # 打印得到的列表









