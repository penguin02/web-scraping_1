# -*- coding: utf-8 -*-
# 031904138-王绮雯
import re

from bs4 import BeautifulSoup, UnicodeDammit
import urllib.request
import requests

#半角字符转全角字符(参考林同学解决方式）
def chartrans(i):
    return chr(ord(i)+65248)

#半角字符串转全角字符串(参考林同学解决方式）
def strtrans(s):
    new=''
    s=str(s)
    for i in s:
        new += chartrans(i)
    return new

# 使用requests爬取网页
def getHTMLText(url):
    try:
        headers = {
            "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.77 Safari/537.36"
        }

        # 使用requests
        req = requests.get(url=url, headers=headers)
        req.encoding = 'utf-8'
        data = req.text
        return data
    except Exception as err:
        print(err)

# 解析html页面内容
def parsePage(html):
    soup = BeautifulSoup(html, 'html.parser')

    # 使用正则表达式提取相关内容
    findrank2020 = re.compile(r'<divclass="ranking"data-v-68e330ae="">(.*?)</div>')
    findlayer = re.compile('</td><tddata-v-68e330ae="">(.*?)<!----></td>')
    findname = re.compile('imgalt="(.*?)"')
    findscore = re.compile('</div></td><tddata-v-68e330ae="">(.*?)</td></tr>')

    try:
        data = []  # 用于记录得到的内容
        for item in soup.find_all('tr')[1:]:
            item = str(item)
            item = re.sub("\r|\n|\\s", "", item)        #先将空格、换行等内容都删除

            rank2020 = re.findall(findrank2020, item)[0]
            layer = re.findall(findlayer, item)[0]
            name = re.findall(findname, item)[0]
            score = re.findall(findscore, item)[0]

            data.append([rank2020,layer,name,score])
        return data

    except Exception as err:
        print(err)

def printlist(list):
    tplt = "{0:^10}\t{1:^10}\t{2:^8}\t{3:^8}\t"     #用于调整输出格式
    print(tplt.format("2020排名", "全部层次", "学校类型", "总分", chr(12288)))
    tplt = "{0:^10}\t{1:^10}\t{2:^10}\t{3:^10}"
    for items in list:
        rank2020 = items[0]
        layer = items[1]
        name = items[2]
        score = items[3]
        print(tplt.format(strtrans(rank2020), layer, name, strtrans(score), chr(12288)))



if __name__ == "__main__":
    url="https://www.shanghairanking.cn/rankings/bcsr/2020/0812"
    html = getHTMLText(url)     #使用requests爬取网页
    list = parsePage(html)      #解析网页内容得到列表
    printlist(list)             #打印得到的列表
