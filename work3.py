# -*- coding: utf-8 -*-
# 031904138-王绮雯
import sqlite3
from urllib import request

from bs4 import BeautifulSoup, UnicodeDammit
import urllib.request
import requests
import re

# 使用urllib.request爬取网页
def getHTMLText(url):
	try:
		headers = {
			# "cookie" : "enc=a7fFeso/9YKxbEuBWRaL6rExLpPSBfyeSM+7yBQmrgQhy3a62jdFEHbk+HsHY3jOsWZlIJInHsATqO3+RHufBQ==; _m_h5_tk=c32f96dc3c400709554cdd1cb918e365_1632044478896; _m_h5_tk_enc=13c06185a5357a27e5ffc2108e3fb546; xlly_s=1; t=47b51e5a14e08ea9f96197fb02f7471e; hng=CN|zh-CN|CNY|156; thw=cn; lLtC1_=1; _uab_collina=163204182482457656540363; cna=pMDwFiMwFBYCAToWcVgw6Da6; lgc=w\u7EEEen; tracknick=w\u7EEEen; mt=ci=91_1; _samesite_flag_=true; cookie2=2eb63cd3ee4760bb702bc645e65d9bb9; _tb_token_=5bee98ee8ee35; sgcookie=E100eELwessWLPNzgogEU2tnpzzZ7NR3idBKvdDdNYLmYq9uJ0xAPsiAF55lXZwpavQBiO0zPs6azsuRiVdBIMihaQh/Xsy9DZG32xH1AGuUU3A=; unb=2961009509; uc1=cookie21=UtASsssmeWzt&pas=0&cookie14=Uoe3dYCDk59MVQ==&cookie15=VFC/uZ9ayeYq2g==&cookie16=Vq8l+KCLySLZMFWHxqs8fwqnEw==&existShop=false; uc3=lg2=UIHiLt3xD8xYTw==&id2=UUGk3/SB8cB0Ng==&vt3=F8dCujd8aevAkgeBpLU=&nk2=FH6Meh8=; csg=f2f5cc23; cancelledSubSites=empty; cookie17=UUGk3/SB8cB0Ng==; dnk=w\u7EEEen; skt=90c084fd9611059a; existShop=MTYzMjEwNjI4NQ==; uc4=nk4=0@FvVZAUPLBO5Y9SmImNnkRA==&id4=0@U2OT74e/gEAOoY5jSfW2V78772GS; _cc_=W5iHLLyFfA==; _l_g_=Ug==; sg=n9b; _nk_=w\u7EEEen; cookie1=UoXlb5m0Yv+g8LRtY2BffH2PI9q02XVmc+XgdmtPWbM=; JSESSIONID=1B2D98C1ED95805A890EF59577CBF4C1; alitrackid=login.taobao.com; lastalitrackid=login.taobao.com; isg=BHl5FgIaPC8wQ9_vM9b1M22oiOVThm04Ll-p8puuQKAfIpm04dbzCOV4pSbUrgVw; tfstk=c1lcBOaanxyb5PeodINjP5vMuuJdZfQ4cJeraX24krv8nqhPifBPLjU1cuom6O1..; l=eBEeoS_uQT7E-_mkBO5Bourza779yIRb4nVzaNbMiInca18PtF1lMNCLZZKJSdtxgtCUUexrwmwghRLHR3AiVh9N33h2q_uq3xvO.".encode("utf-8").decode("latin1"),
			"user-agent" : "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.77 Safari/537.36"
		}
		req = requests.get(url=url, headers=headers)
		req.encoding = 'utf-8'
		data = req.text
		return data
	except Exception as err:
		print(err)

def parsePage(html):    # 解析网页
	try:
		p_picture = re.compile(r'<img src="(.*?)"', re.S)  # 观察html文件内容后提取出关键字,re.S表示忽略换行符
		picturelist = re.findall(p_picture, html)  # 查找图片链接

		list=[]			#用于记录处理后的图片链接（加上 http://news.fzu.edu.cn/才能访问）
		for link in picturelist:
			link = "http://news.fzu.edu.cn" + link
			list.append(link)
		return list
	except:
		return ""

# 输出图片链接列表
def printList(list):
	tply="{0:^4}\t{1:^30}"
	print(tply.format("序号","图片链接",chr(12288)))
	count=1
	for p in list:
		print(tply.format(count,p,chr(12288)))
		count = count + 1

#将每张图片写到本地
def save2file(img_url_list,path):
	i=1
	for img in img_url_list:
		new_path = path + str(i) + ".jpg"
		urllib.request.urlretrieve(img, new_path)		#定义存取本地图片路径
		i += 1


if __name__ == "__main__":
	url = 'http://news.fzu.edu.cn/'

	html = getHTMLText(url)  # 爬取指定网页
	picture_list = parsePage(html)   # 解析网页
	printList(picture_list)         # 输出图片链接列表

	path = r"D:/mymymy!/爬虫实践/图片"	#图片保存路径
	save2file(picture_list,path)		#将图片保存到本地文件


